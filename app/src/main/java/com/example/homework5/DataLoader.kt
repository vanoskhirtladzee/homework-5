package com.example.homework5

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()

    private var service = retrofit.create(Api::class.java)
    fun getRequest(path: String){
        val call =service.getRequest(path)
        call.enqueue(object :Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                d("getRequest", "${t.message}")

            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                d("getRequest", "${response.body()}")            }

        })

    }
}

interface Api {
    @GET("{path}")
    fun getRequest(@Path("path") path: String): Call<String>
}